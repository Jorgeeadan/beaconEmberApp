import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('listar-bloques', 'Integration | Component | listar bloques', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{listar-bloques}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#listar-bloques}}
      template block text
    {{/listar-bloques}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
