import DS from 'ember-data';

export default DS.Transform.extend({
  deserialize(serialized) {
    return {
      nombre: serialized['attributes']['nombre'],
      grupo: serialized['attributes']['grupo'],
    };
  },

  serialize(deserialized) {
    if(deserialized){
      return {
        ['attributes']: deserialized.nombre,
      };
    }
    return null;

  }
});
