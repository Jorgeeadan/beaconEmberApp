import DS from 'ember-data';
import moment from 'moment';

export default DS.Transform.extend({
  deserialize(serialized) {
    return {
      time: moment(serialized).format('h'),
      meridian: moment(serialized).format('a'),
    };
  },

  serialize(deserialized) {
    if (deserialized) {
      return deserialized;
    }
  }
});
