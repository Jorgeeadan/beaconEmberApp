import DS from 'ember-data';
import moment from 'moment';

export default DS.Transform.extend({
  deserialize(serialized) {
    let d = moment(serialized);
    d.locale('es');
    return d.add(1,'hours').calendar();
  },

  serialize(deserialized) {
    if(deserialized){
      return {
        ['attributes']: deserialized.nombre,
      };
    }
    return null;

  }
});
