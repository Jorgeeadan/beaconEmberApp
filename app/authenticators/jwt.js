import Ember from 'ember';
import BaseJWT from 'ember-simple-auth-token/authenticators/jwt';

/**
 The service overrides the JWT service to use a separate "Refresh Token"
 By default the JWT service use the current users JWT access token to
 refresh the access token at a given time period, whereas the TimeManager
 API provides 2 separate tokens in the following format

 {
     "token": "...",
     "refresh_token": "..."
 }

 The access token ("token" in this response) will become invalid after a
 certain amount of time, or when the payload has changed on the backend,
 whereas the refresh token ("refresh_token" in this response) will not.

 This will allows us to update parts of the payload (for example, changing
 a permission string/array/etc), thereby invalidating the current access
 token, and then having the frontend fetch another access token from the
 API automatically rather than requiring the user to re-enter their log
 in details each time
 @see https://auth0.com/blog/refresh-tokens-what-are-they-and-when-to-use-them/


 NOTE: This may not be required as of v3 of ember-simple-auth-token
 @see https://github.com/jpadilla/ember-simple-auth-token#available-customization-options
 */
export default BaseJWT.extend({
  /**
   Get the current time for use in comparison with token age limit
   @returns {number}
   */
  getCurrentTime() {
    return Math.floor((new Date()).getTime() / 1000);
  },

  /**
   Token used to refresh authentication token
   */
  refreshToken: null,

  /**
   * @inheritDoc
   */
  restore(data) {
    this.refreshToken = Ember.get(data, 'refresh_token') || null;
    return this._super(data);
  },

  /**
   Returns a nested object with the token property name.
   Example:  If `tokenPropertyName` is "data.user.token", `makeRefreshData` will return {data: {user: {token: "token goes here"}}}

   @method makeRefreshData
   @return {object} An object with the nested property name.
   */
  makeRefreshData() {
    return {
      refresh_token: this.refreshToken
    };
  },

  /**
   Handles authentication response from server, and returns session data

   @method handleAuthResponse
   @private
   */
  handleAuthResponse(response) {
    this.refreshToken = Ember.get(response, 'refresh_token') || null;

    return this._super(response);
  }
});
