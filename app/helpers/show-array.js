import Ember from 'ember';

export function showArray([arreglo, llave]) {
  let array = arreglo[llave];
  //console.log(array);
  array.sort(sortFunction);
  //console.log(array);
  return array;

  function sortFunction(a, b) {
    if (a.get('hora').time === b.get('hora').time) {
      return 0;
    }
    else {
      return (a.get('hora').time < b.get('hora').time) ? -1 : 1;
    }
  }

}

export default Ember.Helper.helper(showArray);
