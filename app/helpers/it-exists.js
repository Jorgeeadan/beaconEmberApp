import Ember from 'ember';
import { helper } from '@ember/component/helper';

export function itExists([variable, key]) {
  if(variable[key] !== undefined)
  {
    return true;
  }
  return false;
}

export default Ember.Helper.helper(itExists);
