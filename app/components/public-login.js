import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  tagName: '',
  session: service(),
  store: service(),
  isAttemptingLogin: false,
  actions: {
    authenticate() {
      let credentials = this.getProperties('identification', 'password');
      this.set('isAttemptiongLogin', true);

      this.get('session').authenticate('authenticator:jwt', credentials).then(() => {
      }).catch((reason) => {
        let key = `auth.error.${reason.message || reason}`;

        this.set('isAttemptingLogin', false);
      });
    }
  }
});
