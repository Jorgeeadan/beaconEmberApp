import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  ajax: service(),
  store: service(),
  router: service('-routing'),
  init(){
    this._super(...arguments);
    let store = this.get('store');
    let router = this.get('router');
    /*this.get('ajax').request('/mensajes/'+this.get('mensajeId').mensajeId,{
      method: 'DELETE'
    }).then(function (response) {
        console.log(response);
      }
    );*/

    store.findRecord('mensaje', this.get('mensajeId').mensajeId, { backgroundReload: false }).then(function(post) {
      post.destroyRecord();
      router.transitionTo("list-messages"); // => DELETE to /posts/2
    });
  }
});
