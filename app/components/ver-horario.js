import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  store: service(),
  parsedHours: [],
  init(){
    this._super(...arguments);
    let params = this.get('salonId');
    let salonId = params.salonId;
    let flag = params.flag;
    if(salonId){
      this.set('horas', []);
      let store = this.get('store');
      let itself = this;
      if(flag === 'false') {
        store.unloadAll('hora');
      }else{
        itself.set('parsedHours', []);
      }
      store.findRecord('salon', salonId)
        .then( function (salon) {
          itself.set('days',['Lu','Ma','Mi','Ju','Vi','Sa']);
          let hh= salon.get('horario');
          let arrayPromises = [];
          hh.forEach(function (v) {
            arrayPromises.push(
              store.findRecord('hora', v.get('id'))
            );
          });
          Ember.RSVP.all(arrayPromises).then(() => {
            let array = {'Lu':[],'Ma':[],'Mi':[],'Ju':[],'Vi':[],'Sa':[]};
            arrayPromises.forEach(function (horita) {
              array[itself.get('days')[horita.get('weekDay')]].push(horita);
              /*
                            array['Ma'].sort(function (a,b) {
                              if(a.get('hora').meridian === b.get('hora').meridian  )
                              {
                                return (a.get('hora').time - b.get('hora').time)
                              }else{
                                if(a.get('hora').meridian < b.get('hora').meridian  ){
                                  return -1
                                }
                                return 1
                              }
                            });

                            array['Mi'].sort(function (a,b) {
                              if(a.get('hora').meridian === b.get('hora').meridian  )
                              {
                                return (a.get('hora').time - b.get('hora').time)
                              }else{
                                if(a.get('hora').meridian < b.get('hora').meridian  ){
                                  return -1
                                }
                                return 1
                              }
                            });

                            array['Ju'].sort(function (a,b) {
                              if(a.get('hora').meridian === b.get('hora').meridian  )
                              {
                                return (a.get('hora').time - b.get('hora').time)
                              }else{
                                if(a.get('hora').meridian < b.get('hora').meridian  ){
                                  return -1
                                }
                                return 1
                              }
                            });

                            array['Vi'].sort(function (a,b) {
                              if(a.get('hora').meridian === b.get('hora').meridian  )
                              {
                                return (a.get('hora').time - b.get('hora').time)
                              }else{
                                if(a.get('hora').meridian < b.get('hora').meridian  ){
                                  return -1
                                }
                                return 1
                              }
                            });

                            array['Sa'].sort(function (a,b) {
                              if(a.get('hora').meridian === b.get('hora').meridian  )
                              {
                                return (a.get('hora').time - b.get('hora').time)
                              }else{
                                if(a.get('hora').meridian < b.get('hora').meridian  ){
                                  return -1
                                }
                                return 1
                              }
                            });*/
            });

            array['Lu'].sort(function (a,b) {
              if(a.get('hora').meridian === b.get('hora').meridian  )
              {
                return (a.get('hora').time - b.get('hora').time)
              }else{
                if(a.get('hora').meridian < b.get('hora').meridian  ){
                  return -1
                }
                return 1
            }
            });
            itself.set('lunes', array['Lu']);
            itself.set('martes', array['Ma']);
            itself.set('miercoles', array['Mi']);
            itself.set('jueves', array['Ju']);
            itself.set('viernes', array['Vi']);
            itself.set('sabado', array['Sa']);

          });
        });
    }
  }
});
