import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  store: service(),
  ajax: service(),
  salones: [],
  init(){
    this._super(...arguments);
    this.get('store').unloadAll('salon');
    let itself = this;
    this.get('ajax').request('/freeRoom').then(function (response) {
      //
        response.data.forEach(function (v) {
          itself.get('salones').pushObject(itself.get('store').createRecord('salon', {'idFalso':v.id, 'bloque': v.attributes.bloque,'numero': v.attributes.numero}));
        })
      }

    );
  }
});
