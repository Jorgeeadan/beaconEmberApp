import Ember from 'ember';
import { inject as service } from '@ember/service';
import subscribe from 'ember-cordova-events/utils/subscribe';

export default Ember.Component.extend({
  store: service(),
  cordovaEvents: service('ember-cordova/events'),
  beacons:[],
  status: null,
  logReady: subscribe('cordovaEvents.deviceready', function() {
    this.verifyBluetooth();
    let delegate = new cordova.plugins.locationManager.Delegate();
    let store = this.get('store');
    let itself = this;

    delegate.didDetermineStateForRegion = function (pluginResult) {
      if(pluginResult.state === "CLRegionStateInside"){
        store.findRecord('beacon', pluginResult.region.uuid, {include: 'ubicacion'}).then( function (objeto) {
          console.log(objeto);
          itself.set('salon',objeto.get('ubicacion')); // Esto hace una petición :S
          store.queryRecord('hora', {id: itself.get('salon.id'), include:'asignatura', now: 'true'})
            .then( function (hora) {
              itself.set('actualSubject',hora);
            });

          objeto.get('mensajes').then((mensajes)=>{
            itself.set('mensajes', mensajes);
          });
        });
        itself.set('status', 'Estás en el salón ');
      }else{
        itself.set('status',null)
      }
    };
    delegate.didStartMonitoringForRegion = function (pluginResult) {
    };
    delegate.didRangeBeaconsInRegion = function (pluginResult) {
    };
    cordova.plugins.locationManager.setDelegate(delegate);
    // required in iOS 8+
    cordova.plugins.locationManager.requestWhenInUseAuthorization();


  }),
  actualSubject: null,
    init() {
      this._super(...arguments);

      let store = this.get('store');
      let array=[];
      let itself = this;
      store.findAll('beacon').then((beacons=> {
        beacons.forEach(function (beacon) {
          array.push(beacon);

          itself.set('beacons', array);

          itself.startMonitoring();
        });
      }));
      //Funciones de iBeacon
      // or cordova.plugins.locationManager.requestAlwaysAuthorization()
      // Fin de las funciones
    },
    verifyBluetooth(){
      cordova.plugins.locationManager.isBluetoothEnabled()
        .then(function(isEnabled){
          console.log("isEnabled: " + isEnabled);
          if (isEnabled) {
            console.log("isEnabled: " + isEnabled);
          } else {
            cordova.plugins.locationManager.enableBluetooth();
          }
        })
        .fail(function(e) { console.error(e); })
        .done();
    },

    startMonitoring(){
    let faros = this.get('beacons');
    faros.forEach(function (faro) {
      let beaconRegion = new cordova.plugins.locationManager.BeaconRegion(faro.id, faro.id, faro.get('major'), faro.get('minor'));
      cordova.plugins.locationManager.startMonitoringForRegion(beaconRegion)
        .fail(function(e) { console.error(e); })
        .done(function(e) {
          }
        );
    })
  },
    startRanging(){
    let faros = this.get('beacons');
    faros.forEach(function (faro) {
      let beaconRegion = new cordova.plugins.locationManager.BeaconRegion(faro.id, faro.id, faro.get('major'), faro.get('minor'));
      cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion)
        .fail(function(e) { console.error(e); })
        .done();
    })
  },
    stopMonitoring(){
    let faros = this.get('beacons');
    faros.forEach(function (faro) {
      let beaconRegion = new cordova.plugins.locationManager.BeaconRegion(faro.id, faro.id, faro.get('major'), faro.get('minor'));
      cordova.plugins.locationManager.stopMonitoringForRegion(beaconRegion)
        .fail(function(e) { console.error(e); })
        .done();
    })
  },
    stopRanging(){
    let faros = this.get('beacons');
    faros.forEach(function (faro) {
      let beaconRegion = new cordova.plugins.locationManager.BeaconRegion(faro.id, faro.id, faro.get('major'), faro.get('minor'));
      cordova.plugins.locationManager.stopRangingBeaconsInRegion(beaconRegion)
        .fail(function(e) { console.error(e); })
        .done();
    })
  }
});
