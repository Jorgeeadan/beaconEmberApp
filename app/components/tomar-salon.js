import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  ajax: service(),
  store: service(),
  init(){
    this._super(...arguments);
    let store = this.get('store');

    let itself = this;
    this.get('ajax').request('/takeRoom/'+this.get('salonId').salonId).then(function (response) {
      console.log(response);
      store.findRecord('salon', itself.get('salonId').salonId, { backgroundReload: false }).then(function(post) {
        post.deleteRecord();
      });
    });


  }
});
