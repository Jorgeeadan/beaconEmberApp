import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  store: service(),
  init(){
    this._super(...arguments);

    this.set('mensajes', []);
    this.set('sinMensajes', false);
    let store = this.get('store');
    let array=[];
    let itself = this;
    store.unloadAll('mensaje');
    store.findAll('mensaje').then((mensajes)=>{
      if(mensajes.content.length === 0)
      {
        store.findAll('mensaje').then((mensajes2)=>{
          if(mensajes2.content.length === 0)
          {
            itself.set('sinMensajes', true);
          }
          mensajes2.forEach(function (mensaje){
            array.push(mensaje);
          });
          itself.set('mensajes', null);
          itself.set('mensajes', array);
        });
      }else {
        mensajes.forEach(function (mensaje) {
          array.push(mensaje);
        });
        itself.set('mensajes', null);
        itself.set('mensajes', array);
      }

    });
  }
});
