import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  store: service(),
  bloques: null,
  init(){
    this._super(...arguments);

    this.set('bloques', []);
    let store = this.get('store');
    let array=[];
    let itself = this;
    store.unloadAll('salon');
    store.findAll('salon').then((salons=>{
      salons.forEach(function (salon) {
        let bloque = salon.get('bloque');
        if (!array.includes(bloque))
          array.push(bloque);
      });
      itself.set('bloques', array);
      /*if(salons.content.length === 0)
      {
        store.findAll('salon').then((salons2=>{
          salons2.forEach(function (salon){
            let bloque = salon.get('bloque');
            if(!array.includes(bloque))
              array.push(bloque);
          });
          itself.set('bloques', array);

        }))
      }else {
        salons.forEach(function (salon) {
          let bloque = salon.get('bloque');
          if (!array.includes(bloque))
            array.push(bloque);
        });
        itself.set('bloques', array);
      }*/
    }));
  }
});
