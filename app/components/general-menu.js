import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  tagName : '',
  session: service(),
  actions: {
    logout() {
      this.get('session').invalidate();
    }
  }
});
