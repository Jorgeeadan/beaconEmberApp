import Ember from 'ember';
import { inject as service } from '@ember/service';
import EmberObject, { computed } from '@ember/object';
import moment from 'moment';

export default Ember.Component.extend({
  store: service(),
  router: service('-routing'),
  init(){
    this._super(...arguments);

    let store = this.get('store');
    this.set('now',  new Date());
    let salonId = this.get('salonId');
    let mensajeId = this.get('mensajeId');
    console.log(salonId);
    if(salonId){
      let salon = store.peekRecord('salon', salonId);
      this.set('mensaje', store.createRecord('mensaje', {
        'salon': salon,
        'detalle': null,
        'duracion': null
      }))
    }

    if(mensajeId)
    {
      let itself = this;
      let mensaje = store.findRecord('mensaje', mensajeId).then(function (mensaje){
        itself.set('mensaje', mensaje);
      });
      this.set('salones', []);

      let salons = store.findAll('salon').then(function (salones) {
          itself.set('salones', salones);
      })
    }
  },
  actions:{
    guardarMensaje(){
      let router = this.get('router');
      let mensaje = this.get('mensaje');
      let store = this.get('store');
      if(this.get('mensajeId'))
      {
        let nuevoMensaje = store.createRecord('mensaje', {
          'salon': mensaje.get('salon'),
          'detalle': mensaje.get('detalle'),
          'duracion': mensaje.get('duracion'),
          'republicado': true
        });

        nuevoMensaje.save().then(function (response) {
          router.transitionTo("list-messages");
        }).catch(function (failure) {
          console.log(failure)
        });

      }else {
        this.get("mensaje").save().then(function (response) {
          router.transitionTo("list-messages");
        }).catch(function (failure) {
          console.log(failure)
        });
      }
    },
    changeSalon(){

    }
  }
});
