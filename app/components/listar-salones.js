import Ember from 'ember';
import { inject as service } from '@ember/service';

export default Ember.Component.extend({
  store: service(),
  salones: null,
  init(){
    this._super(...arguments);
    let bloque = this.get('bloque');
    this.set('salones', []);
    let store = this.get('store');
    let array=[];
    let itself = this;
    let salones = store.peekAll('salon');
    array = salones.filter(function (salon){
      return salon.get('bloque') === bloque;
    });
    itself.set('salones', array);
  }
});
