import DS from 'ember-data';

export default DS.Model.extend({
  salon: DS.belongsTo('salon'),
  hora: DS.attr('string-to-time'),
  weekDay: DS.attr('number'),
  asignaturaId: DS.attr('asignatura-id'),
  docente: DS.attr('docente')
});
