import DS from 'ember-data';

export default DS.Model.extend({
  email: DS.attr('string'),
  username: DS.attr('string'),
  fullName: DS.attr('full-name'),
  plainPassword: DS.attr('string'),
  role: DS.attr('string')
});
