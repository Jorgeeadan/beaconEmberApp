import DS from 'ember-data';

export default DS.Model.extend({
  bloque: DS.attr('string'),
  numero: DS.attr('string'),
  mapa: DS.attr('string'),
  idFalso: DS.attr('number'),
  horario: DS.hasMany('hora', {
    inverse: 'salon'} )
});
