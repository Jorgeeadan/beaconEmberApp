import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  autor: DS.attr('docente'),
  beacon: DS.belongsTo('beacon'),
  salon: DS.belongsTo('salon'),
  date: DS.attr(''),
  time: DS.attr(''),
  /*fechaVisibilidad: computed('date', 'time', function() {
    return `${this.get('date')} ${this.get('time')}`;
  }),*/
  fechaVisibilidad: DS.attr('fecha-mensaje'),
  detalle: DS.attr('string'),
  duracion: DS.attr('number'),
  republicado: DS.attr('boolean')
});
