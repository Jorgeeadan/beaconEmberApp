import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  major: DS.attr('number'),
  minor: DS.attr('number'),
  mensajes: DS.hasMany('mensaje'),
  ubicacion: DS.belongsTo('salon', {async: true})
});
