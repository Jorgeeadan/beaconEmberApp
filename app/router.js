import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');
  this.route('new-message');
  this.route('list-messages');
  this.route('free-room');
  this.route('listas-salones', { path: 'bloques/:bloque' });
  this.route('create-message', { path: 'mensaje/:salonId'});
  this.route('duplica-mensaje', {path: 'republicar/:mensajeId'});
  this.route('show-schedule', {path: 'schedule/:salonId/:flag'});
  this.route('take-room', {path: 'take/:salonId'});
  this.route('elimina-mensaje', {path: 'elimina/:mensajeId'});
});

export default Router;
